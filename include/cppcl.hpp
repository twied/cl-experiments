// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef CPPCL_H_
#define CPPCL_H_

#ifndef CL_TARGET_OPENCL_VERSION
#define CL_TARGET_OPENCL_VERSION 120
#endif

#if CL_TARGET_OPENCL_VERSION < 120
#error need OpenCL version 1.2
#endif

#include <CL/opencl.h>

#include <string>

namespace cppcl {

std::string err_str(cl_int err) noexcept {
    switch (-err) {
    case 0x00:
        return "CL_SUCCESS";
    case 0x01:
        return "CL_DEVICE_NOT_FOUND";
    case 0x02:
        return "CL_DEVICE_NOT_AVAILABLE";
    case 0x03:
        return "CL_COMPILER_NOT_AVAILABLE";
    case 0x04:
        return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case 0x05:
        return "CL_OUT_OF_RESOURCES";
    case 0x06:
        return "CL_OUT_OF_HOST_MEMORY";
    case 0x07:
        return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case 0x08:
        return "CL_MEM_COPY_OVERLAP";
    case 0x09:
        return "CL_IMAGE_FORMAT_MISMATCH";
    case 0x0a:
        return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case 0x0b:
        return "CL_BUILD_PROGRAM_FAILURE";
    case 0x0c:
        return "CL_MAP_FAILURE";
    case 0x0d:
        return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case 0x0e:
        return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case 0x0f:
        return "CL_COMPILE_PROGRAM_FAILURE";
    case 0x10:
        return "CL_LINKER_NOT_AVAILABLE";
    case 0x11:
        return "CL_LINK_PROGRAM_FAILURE";
    case 0x12:
        return "CL_DEVICE_PARTITION_FAILED";
    case 0x13:
        return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
    case 0x1e:
        return "CL_INVALID_VALUE";
    case 0x1f:
        return "CL_INVALID_DEVICE_TYPE";
    case 0x20:
        return "CL_INVALID_PLATFORM";
    case 0x21:
        return "CL_INVALID_DEVICE";
    case 0x22:
        return "CL_INVALID_CONTEXT";
    case 0x23:
        return "CL_INVALID_QUEUE_PROPERTIES";
    case 0x24:
        return "CL_INVALID_COMMAND_QUEUE";
    case 0x25:
        return "CL_INVALID_HOST_PTR";
    case 0x26:
        return "CL_INVALID_MEM_OBJECT";
    case 0x27:
        return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case 0x28:
        return "CL_INVALID_IMAGE_SIZE";
    case 0x29:
        return "CL_INVALID_SAMPLER";
    case 0x2a:
        return "CL_INVALID_BINARY";
    case 0x2b:
        return "CL_INVALID_BUILD_OPTIONS";
    case 0x2c:
        return "CL_INVALID_PROGRAM";
    case 0x2d:
        return "CL_INVALID_PROGRAM_EXECUTABLE";
    case 0x2e:
        return "CL_INVALID_KERNEL_NAME";
    case 0x2f:
        return "CL_INVALID_KERNEL_DEFINITION";
    case 0x30:
        return "CL_INVALID_KERNEL";
    case 0x31:
        return "CL_INVALID_ARG_INDEX";
    case 0x32:
        return "CL_INVALID_ARG_VALUE";
    case 0x33:
        return "CL_INVALID_ARG_SIZE";
    case 0x34:
        return "CL_INVALID_KERNEL_ARGS";
    case 0x35:
        return "CL_INVALID_WORK_DIMENSION";
    case 0x36:
        return "CL_INVALID_WORK_GROUP_SIZE";
    case 0x37:
        return "CL_INVALID_WORK_ITEM_SIZE";
    case 0x38:
        return "CL_INVALID_GLOBAL_OFFSET";
    case 0x39:
        return "CL_INVALID_EVENT_WAIT_LIST";
    case 0x3a:
        return "CL_INVALID_EVENT";
    case 0x3b:
        return "CL_INVALID_OPERATION";
    case 0x3c:
        return "CL_INVALID_GL_OBJECT";
    case 0x3d:
        return "CL_INVALID_BUFFER_SIZE";
    case 0x3e:
        return "CL_INVALID_MIP_LEVEL";
    case 0x3f:
        return "CL_INVALID_GLOBAL_WORK_SIZE";
    case 0x40:
        return "CL_INVALID_PROPERTY";
    case 0x41:
        return "CL_INVALID_IMAGE_DESCRIPTOR";
    case 0x42:
        return "CL_INVALID_COMPILER_OPTIONS";
    case 0x43:
        return "CL_INVALID_LINKER_OPTIONS";
    case 0x44:
        return "CL_INVALID_DEVICE_PARTITION_COUNT";
    default:
        break;
    }

    return "Error " + std::to_string(err);
}

} /* namespace cppcl */

#endif /* CPPCL_H_ */
